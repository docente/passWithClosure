//
//  ViewController.swift
//  PassWithClosure
//
//  Created by paco on 29/10/17.
//  Copyright © 2017 TCB. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! ViewController2).callBack = { (result) in
            print(result)
            self.label.text = result
        }
    }

}

