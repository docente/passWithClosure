//
//  ViewController2.swift
//  PassWithClosure
//
//  Created by paco on 29/10/17.
//  Copyright © 2017 TCB. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet var textField: UITextField!
    var callBack: ((String) ->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func Done() {
        callBack?(textField.text!)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
